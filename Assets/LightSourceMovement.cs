﻿using UnityEngine;
using System.Collections;

public class LightSourceMovement : MonoBehaviour {

    public GameObject terrain;
    private float angulerSpeed = 0.2f;

	// Update is called once per frame
	void Update () {

        Vector3 rotateCenter;
        float centerX = terrain.transform.position.x ;
        float centerZ = terrain.transform.position.z ;
        rotateCenter = new Vector3(centerX, 0, centerZ);
        this.transform.RotateAround(rotateCenter, Vector3.forward, angulerSpeed);

        this.transform.LookAt(terrain.transform);
        
        

    }
}
