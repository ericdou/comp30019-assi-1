﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq; // used for Sum of array



public class MapGenerator : MonoBehaviour
{

    //private static int L = 257;
    private static int MAX_H = 500;
    public float roughness ;
    public int seed = 0;

    public Shader shader;
    public Terrain t;

    private MeshFilter waterFilter;
    private MeshRenderer waterRenderer;

    public Texture2D grass;
    public Texture2D snow;
    public Texture2D soil;
    




    // Use this for initialization
    void Start()
    {
        TerrainData td = new TerrainData();

        GameObject map = generateMap(td);

        waterFilter = this.gameObject.AddComponent<MeshFilter>();
        //waterFilter.mesh = this.createWaterMesh();

        waterRenderer = this.gameObject.AddComponent<MeshRenderer>();
        //waterRenderer.material = default (Material);



        //Component t = map.GetComponent<Terrain>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    GameObject generateMap(TerrainData td)
    {
        int L = (int)t.terrainData.size.x;
        td.alphamapResolution = L;
        td.heightmapResolution = L;
        td.name = "MapData";
        td.size = new Vector3(L, MAX_H, L);
        Vector3 posi = new Vector3(-L/2, 0, -L/2);

        float[,] heightmap = new float[L, L];

        float c1, c2, c3, c4;

        //Assign the four corners of the intial grid random color values
        //These will end up being the colors of the four corners of the applet.     
        c1 = 0f;
        c2 = 0f;
        c3 = 0f;
        c4 = 0f;

        divideGrid(0.0f, 0.0f, L, L,c1, c2, c3, c4, heightmap, 0.5f);


        //diamondSquare(heightmap);
        td.SetHeights(0, 0, heightmap);
        assignAlphaMap(td);

        var map = Terrain.CreateTerrainGameObject(td);

        map.transform.position = posi;
        t = map.GetComponent<Terrain>();
       
        t.Flush();



        return map;
    }

    /* Diamond-square-algorithms code from http://stackoverflow.com/questions/2755750/diamond-square-algorithm
     * 
     */
    void diamondSquare(float[,] heightmap)
    {
        int size = heightmap.GetLength(1);
        heightmap[0, size - 1] = 0;
        heightmap[size - 1, size - 1] = 0;
        heightmap[size - 1, 0] = 0;
        heightmap[0, 0] = 0;
        
        //Random.seed = seed;
        //the range (-h -> +h) for the average offset
        float h = 0.2f;

        for (int sideLength = size - 1;
            //side length must be >= 2 so we always have
            //a new value (if its 1 we overwrite existing values
            //on the last iteration)
            sideLength >= 2;
            //each iteration we are looking at smaller squares
            //diamonds, and we decrease the variation of the offset
            sideLength /= 2, h /= roughness)
        {
            //half the length of the side of a square
            //or distance from diamond center to one corner
            //(just to make calcs below a little clearer)
            int halfSide = sideLength / 2;

            //generate the new square values
            for (int x = 0; x < size - 1; x += sideLength)
            {
                for (int y = 0; y < size - 1; y += sideLength)
                {
                    float leftBound = 1, rightBound = 1, upBound = 1, lowBound = 1;
                    if (x + halfSide == 0) leftBound = 0;
                    if (x + halfSide == size-1) rightBound = 0;
                    if (y + halfSide ==0) upBound = 0;
                    if (y + halfSide == size - 1) lowBound = 0;
                    //x, y is upper left corner of square
                    //calculate average of existing corners
                    float avg = heightmap[x, y] *leftBound*upBound + //top left
                    heightmap[x + sideLength, y] * rightBound*upBound +//top right
                    heightmap[x, y + sideLength] * leftBound*lowBound+ //lower left
                    heightmap[x + sideLength, y + sideLength] * rightBound*lowBound;//lower right
                    avg /= (leftBound+rightBound+upBound+lowBound);

                    //center is average plus random offset
                    heightmap[x + halfSide, y + halfSide] =
                  //We calculate random value in range of 2h
                  //and then subtract h so the end value is
                  //in the range (-h, +h)
                  avg + (Random.value * 2 * h) - h;
                }
            }

            //generate the diamond values
            //since the diamonds are staggered we only move x
            //by half side
            //NOTE: if the data shouldn't wrap then x < DATA_SIZE
            //to generate the far edge values
            for (int x = 0; x < size; x += halfSide)
            {
                //and y is x offset by half a side, but moved by
                //the full side length
                //NOTE: if the data shouldn't wrap then y < DATA_SIZE
                //to generate the far edge values
                for (int y = (x + halfSide) % sideLength; y < size; y += sideLength)
                {

                    float leftBound = 1, rightBound = 1, upBound = 1, lowBound = 1;
                    if (x  == 0) leftBound = 0;
                    if (x  == size - 1) rightBound = 0;
                    if (y  == 0) upBound = 0;
                    if (y  == size - 1) lowBound = 0;
                    //x, y is center of diamond
                    //note we must use mod  and add DATA_SIZE for subtraction 
                    //so that we can wrap around the array to find the corners
                    float avg =
                      heightmap[(x - halfSide + size) % size, y] *leftBound+ //left of center
                      heightmap[(x + halfSide) % size, y] *rightBound+ //right of center
                      heightmap[x, (y + halfSide) % size] *lowBound+ //below center
                      heightmap[x, (y - halfSide + size) % size] * upBound; //above center
                    avg /= (leftBound + rightBound + upBound + lowBound);

                    //new value = average plus random offset
                    //We calculate random value in range of 2h
                    //and then subtract h so the end value is
                    //in the range (-h, +h)
                    avg = avg + (Random.value * 2 * h) - h;
                    //update value for center of diamond
                    heightmap[x, y] = avg;
                }
            }
        }

    }



    void assignAlphaMap(TerrainData td)
    {

        var grassT = new SplatPrototype();
        var snowT = new SplatPrototype();
        var soilT = new SplatPrototype();
        grassT.texture = grass;
        snowT.texture = snow;
        soilT.texture = soil;

        td.splatPrototypes = new SplatPrototype[] { grassT, snowT, soilT };

        float[,,] splatmapData = new float[td.alphamapWidth, td.alphamapHeight, td.alphamapLayers];

        for (int y = 0; y < td.alphamapHeight; y++)
        {
            for (int x = 0; x < td.alphamapWidth; x++)
            {
                
                // Normalise x/y coordinates to range 0-1 
                float y_01 = (float)y / (float)td.alphamapHeight;
                float x_01 = (float)x / (float)td.alphamapWidth;

                // Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
                float height = td.GetHeight(Mathf.RoundToInt(y_01 * td.heightmapHeight), Mathf.RoundToInt(x_01 * td.heightmapWidth));

               
                
                
                // Setup an array to record the mix of texture weights at this point
                float[] splatWeights = new float[td.alphamapLayers];

                // CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT

                splatWeights[0] = 0.2f;

                if (height < 50.0f) splatWeights[1] = 0;
                else
                {
                    splatWeights[0] = 0;
                    splatWeights[1] = 0.5f;
                }
                if(height < 1f)
                splatWeights[2] = 0.3f;

                // Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
                float z = splatWeights.Sum();
                
                // Loop through each terrain texture
                for (int i = 0; i < td.alphamapLayers; i++)
                {

                    // Normalize so that sum of all texture weights = 1
                    splatWeights[i] /= z;

                    // Assign this point to the splatmap array
                    splatmapData[x, y, i] = splatWeights[i];

                }
                


            }
            td.SetAlphamaps(0, 0, splatmapData);
        }
        
    }




    /* Original code from http://unitycoder.com/blog/2012/04/03/diamond-square-algorithm/
     * Recursive diamond square algorithm
     * 
     */
    void divideGrid(float x, float y, float w, float h, float c1, float c2, float c3, float c4, float[,] heightmap, float range)
    {

        float newWidth = w * 0.5f;
        float newHeight = h * 0.5f;

        if (w < 1.0f || h < 1.0f)
        {
            //The four corners of the grid piece will be averaged and drawn as a single pixel.
            float c = (c1 + c2 + c3 + c4) * 0.25f;
            heightmap[(int)x, (int)y] = c;
        }
        else
        {
            float middle = (c1 + c2 + c3 + c4) * 0.25f + Random.Range(-0.5f, 0.5f) * range;      //Randomly displace the midpoint!
            float edge1 = (c1 + c2) * 0.5f; //Calculate the edges by averaging the two corners of each edge.
            float edge2 = (c2 + c3) * 0.5f;
            float edge3 = (c3 + c4) * 0.5f;
            float edge4 = (c4 + c1) * 0.5f;

        
            //Do the operation over again for each of the four new grids.                 
            divideGrid(x, y, newWidth, newHeight, c1, edge1, middle, edge4, heightmap, range / roughness);
            divideGrid(x + newWidth, y, newWidth, newHeight, edge1, c2, edge2, middle, heightmap, range / roughness);
            divideGrid(x + newWidth, y + newHeight, newWidth, newHeight, middle, edge2, c3, edge3, heightmap, range / roughness);
            divideGrid(x, y + newHeight, newWidth, newHeight, edge4, middle, edge3, c4, heightmap, range / roughness);
        }
    }


    Mesh createWaterMesh()
    {
        Mesh water = new Mesh();
        water.name = "Water";
        int size = (int)t.terrainData.size.x;
        water.vertices = new Vector3[size * size];
        water.colors = new Color[size * size];
        int count = 0;
        for(int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                water.vertices[size * x + y] = new Vector3(x - size / 2, 1, y - size / 2);
                water.colors[size * x + y] = Color.blue;

            }
           
            
        }

        water.RecalculateNormals();


        water.triangles = new int[(size - 1) * (size - 1) * 2 * 3];
        // triangles
        for (int x = 1; x < size; x++)
        {
            for (int y = 0; y < size - 1; y++)
            {
                // taking the 2 triangles of each square

                // triangle 1
                water.triangles[count] = size * x + y;                 // bottom left
                water.triangles[count + 1] = size * (x - 1) + y;       // top left
                water.triangles[count + 2] = size * (x - 1) + (y + 1); // top right

                // triangle 2
                water.triangles[count + 3] = size * x + y;             // bottom left
                water.triangles[count + 4] = size * (x - 1) + (y + 1); // top right
                water.triangles[count + 5] = size * x + (y + 1);       // bottom right
                count += 6;
            }
        }

        


        return water;
    }
}


